package com.example.enesyildirim.tablayoutviewer.Datas;

import android.support.v7.widget.RecyclerView;

import java.io.Serializable;
import java.util.List;

public class MainData implements Serializable {

    private CityProperties city;
    private List<DailyValues> list;

    public CityProperties getCity() {
        return city;
    }

    public void setCity(CityProperties city) {
        this.city = city;
    }

    public List<DailyValues> getList() {
        return list;
    }

    public void setList(List<DailyValues> list) {
        this.list = list;
    }


}
