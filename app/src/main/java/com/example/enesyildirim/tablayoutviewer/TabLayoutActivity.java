package com.example.enesyildirim.tablayoutviewer;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.example.enesyildirim.tablayoutviewer.Drawer.CreateDrawerHeader;
import com.example.enesyildirim.tablayoutviewer.Drawer.DrawerMenuItems;
import com.example.enesyildirim.tablayoutviewer.Fragments.ViewPagerFragment;

public class TabLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_layout);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayout, new ViewPagerFragment(), "ViewPager");
        transaction.addToBackStack("ViewPager");
        transaction.commit();
    }

}
