package com.example.enesyildirim.tablayoutviewer.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.enesyildirim.tablayoutviewer.Adapters.TabLayoutAdapter;
import com.example.enesyildirim.tablayoutviewer.R;

public class ViewPagerFragment extends Fragment{
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_fragment,container,false);
        ViewPager viewPager = view.findViewById(R.id.viewPager);
        setupPageView(viewPager);
        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.current_loc);
        tabLayout.getTabAt(1).setIcon(R.drawable.search_fra);
        tabLayout.getTabAt(2).setIcon(R.drawable.spesific_city);
        return view;
    }

    private void setupPageView(ViewPager viewPager) {
        TabLayoutAdapter tabLayoutAdapter = new TabLayoutAdapter(getActivity().getSupportFragmentManager());
        tabLayoutAdapter.addLayout(new CurrentCityWeatherFragment());
        tabLayoutAdapter.addLayout(new SearchFragment());
        tabLayoutAdapter.addLayout(new SpesificCityFragment());
        viewPager.setAdapter(tabLayoutAdapter);
    }
}