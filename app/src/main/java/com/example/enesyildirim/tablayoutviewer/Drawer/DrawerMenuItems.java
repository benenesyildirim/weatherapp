package com.example.enesyildirim.tablayoutviewer.Drawer;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.enesyildirim.tablayoutviewer.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@Layout(R.layout.drawer_item)
public class DrawerMenuItems {
    public static final int Item1 = 1;
    public static final int Item2 = 2;
    public static final int Item3 = 3;
    public static final int Item4 = 4;

    private int mMenuPosition;
    private Context mContext;
    private DrawerCallBack mCallBack;

    public DrawerMenuItems(Context context, int menuPosition) {
        mContext = context;
        mMenuPosition = menuPosition;
    }

    @View(R.id.itemNameTxt)
    private TextView itemNameTxt;

    @View(R.id.itemIcon)
    private ImageView itemIcon;

    @Resolve
    private void onResolved() {
        switch (mMenuPosition) {
            case Item1:
                itemNameTxt.setText("Item1");
                itemIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.menu_icon));
                break;
            case Item2:
                itemNameTxt.setText("Item2");
                itemIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.search_icon));
                break;
            case Item3:
                itemNameTxt.setText("Item3");
                itemIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.celcius_icon));
                break;
            case Item4:
                itemNameTxt.setText("Item4");
                itemIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.weather_icon));
                break;
        }
    }

    @Click(R.id.mainView)
    private void onMenuItemClick() {
        switch (mMenuPosition) {
            case Item1:
                Toast.makeText(mContext, "Item1", Toast.LENGTH_SHORT).show();
                if (mCallBack != null)
                    mCallBack.Item1Listener();
                break;
            case Item2:
                Toast.makeText(mContext, "Item2", Toast.LENGTH_SHORT).show();
                if (mCallBack != null)
                    mCallBack.Item2Listener();
                break;
            case Item3:
                Toast.makeText(mContext, "Item3", Toast.LENGTH_SHORT).show();
                if (mCallBack != null)
                    mCallBack.Item3Listener();
                break;
            case Item4:
                Toast.makeText(mContext, "Item4", Toast.LENGTH_SHORT).show();
                if (mCallBack != null)
                    mCallBack.Item4Listener();
                break;
        }
    }

    public void setDrawerCallBack(DrawerCallBack callBack) {
        mCallBack = callBack;
    }

    public interface DrawerCallBack {
        void Item1Listener();

        void Item2Listener();

        void Item3Listener();

        void Item4Listener();
    }
}