package com.example.enesyildirim.tablayoutviewer.ApiTools;

import com.example.enesyildirim.tablayoutviewer.Datas.MainData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RequestInterface {
    @GET("data/2.5/forecast/daily?units=metric&APPID=ea574594b9d36ab688642d5fbeab847e")
    Call<MainData> getWeather(@Query("q") String cityName, @Query("cnt") String day);
}
