package com.example.enesyildirim.tablayoutviewer.Drawer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.enesyildirim.tablayoutviewer.R;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.NonReusable;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

public class CreateDrawerHeader extends android.view.View {
    public CreateDrawerHeader(Context context) {
        super(context);
    }

    @NonReusable
    @Layout(R.layout.drawer_header)
    public class DrawerHeader {

        @View(R.id.profileImage)
        private ImageView profileImage;

        @View(R.id.profileNameText)
        private TextView nameTxt;

        @View(R.id.profileEmailText)
        private TextView emailTxt;

        @SuppressLint("SetTextI18n")
        @Resolve
        private void onResolved() {
            nameTxt.setText("KarpuzAtan");
            emailTxt.setText("enesyildirim@n11.com");
        }
    }
}
