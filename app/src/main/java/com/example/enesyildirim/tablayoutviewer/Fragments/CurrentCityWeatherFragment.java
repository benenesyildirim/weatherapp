package com.example.enesyildirim.tablayoutviewer.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.enesyildirim.tablayoutviewer.Adapters.RecycleViewAdapter;
import com.example.enesyildirim.tablayoutviewer.ApiTools.API;
import com.example.enesyildirim.tablayoutviewer.ApiTools.RequestInterface;
import com.example.enesyildirim.tablayoutviewer.Datas.CityTempatures;
import com.example.enesyildirim.tablayoutviewer.Datas.MainData;
import com.example.enesyildirim.tablayoutviewer.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentCityWeatherFragment extends Fragment implements LocationListener, RecycleViewAdapter.RowListener {

    private String cityName;
    private View view;
    private RecyclerView recyclerView;
    private RecycleViewAdapter recycleVAdapter;

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.current_city_properties, container, false);
        takeLocation();

        RequestInterface requestInterface = API.getClient().create(RequestInterface.class);
        Call<MainData> call = requestInterface.getWeather(cityName, "5");

        call.enqueue(new Callback<MainData>() {
            @Override
            public void onResponse(Call<MainData> call, Response<MainData> response) {
                if (response.isSuccessful()) {
                    recyclerView = view.findViewById(R.id.recyclerView);
                    recycleVAdapter = new RecycleViewAdapter(response.body(), CurrentCityWeatherFragment.this);
                    recyclerView.setAdapter(recycleVAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                } else {
                    Toast.makeText(getContext(),"Can't Find Location!",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MainData> call, Throwable t) {
                Toast.makeText(getContext(),"Can't Find Location!",Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

    private void takeLocation() {//TODO Şu kısmın da bir açıklamasını alabilir miyim?
        LocationManager locManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        int coarseLocation = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int fineLocation = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (coarseLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (fineLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
        }

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        String provider = locManager.getBestProvider(criteria, false);
        Location location = locManager.getLastKnownLocation(provider);

        List<String> providerList = locManager.getAllProviders();
        if (location != null && providerList != null && providerList.size() > 0) {
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (null != listAddresses && listAddresses.size() > 0) {
                    cityName = listAddresses.get(0).getSubAdminArea();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onClickRowListener(MainData mainData) {
        DetailOfRowFragment detailOfRow = new DetailOfRowFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("weatherDetail", mainData);
        detailOfRow.setArguments(bundle);

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayout, detailOfRow, "DetailItems");
        transaction.addToBackStack("DetailItems");
        transaction.commit();

    }
}